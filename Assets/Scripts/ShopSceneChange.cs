using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopSceneChange : GlobalCount
{
    private void Update(){
        InternalHatCount = HatCount;
        HatDisplay.GetComponent<Text>().text = "Christmas Hats: " + InternalHatCount; //Carry over the internal value when scene changes.
    }
}
