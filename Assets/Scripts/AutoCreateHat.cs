using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoCreateHat : GlobalCount
{

    public bool creatingHat = false;
    public static int CountIncrease = 1;
    public int InternalCountIncrease;

    void Update(){
        InternalCountIncrease = CountIncrease;
        if(creatingHat == false){
            creatingHat = true;
            StartCoroutine(CreateHat());
        }
    }

    IEnumerator CreateHat(){
        yield return new WaitForSeconds(1);
        GlobalCount.HatCount += InternalCountIncrease;
        creatingHat = false;
    }
    
}
