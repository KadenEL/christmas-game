using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalCount : MonoBehaviour
{
    public static int HatCount;
    public GameObject HatDisplay;
    public int InternalHatCount;

    private void Update(){
        InternalHatCount = HatCount;
        HatDisplay.GetComponent<Text>().text = "Christmas Hats: " + InternalHatCount; //Store internal value when scene changes.
    }
}
