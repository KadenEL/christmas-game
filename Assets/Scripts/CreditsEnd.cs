using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CreditsEnd : MonoBehaviour
{
    public void ReloadGame()
    {
        SceneManager.LoadScene("MainMenu"); //Once credits is over this switches back to main menu (Shouldn't need to edit)
    }
}